$(document).ready(function() {
	$('.slider_on_main').slick({
	  infinite: true,
	  slidesToShow: 1,
	  slidesToScroll: 1,
      arrows: true,
    prevArrow: '<div class="slick-prev"><img src="img/slider_left.png" alt="slider_arrow"/></div>',
    nextArrow: '<div class="slick-next"><img src="img/slider_right.png" alt="slider_arrow"/></div>'
	});
    $("#do_contacts_feedback").click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        $("#contacts_feedback").toggle(300);
        setTimeout(function() {
            $("#feedback_contacts_success").css('display','inline-block');
        }, 100)
    });
});

